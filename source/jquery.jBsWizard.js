/**
 * A jquery plugin to add Wizard functionality for Bootstrap 3 Tabs
 * Author: Jan Doll (https://www.xing.com/profile/Jan_Doll2)
 * Licensed under the MIT license
 */

if(typeof jQuery === 'undefined') {
    console.warn('jBsWizard Warning:', 'Make sure jQuery is included before running jBsWizard');
}

(function ( $ ) {

    'use strict';

    var settings;

    $.fn.jBsWizard = function ( options ) {

        /** Check if set to multiple Elements */
        if (this.length > 1){
            this.each(function() { $(this).jBsWizard( options ) });
            return this;
        }

        /** Set Settings */
        settings = $.extend({
            nextBtnStr             : 'Next',     /** String: Will be used for creating Next Button */
            nextBtnAdditionalClass : '',         /** String: Additional Classes for Next Button, separated by space */
            prevBtnStr             : 'Previous', /** String: Will be used for creating Previous Button */
            prevBtnAdditionalClass : '',         /** String: Additional Classes for Previous Button, separated by space */
            waitASecondDelay       : 1000,       /** Integer: Set Milliseconds for Fake Process Time */
            processingFadeSpeed    : 'fast',     /** String | Integer: 'fast', 'slow' or Integer , see http://api.jquery.com/fadeTo/ */
            useProgressBar         : true        /** Boolean: For Showing and using of the Progress Bar */
        }, options);

        /** Set additional Settings */
        var thisWizardObj = $(this);

        /** Set Instance for later Use */
        var instance = this;

        /** Internal Method: Set Step Information */
        var _setSteps = function() {

            /** Set Wizard Step Elements */
            var wizardSteps = thisWizardObj.find('ul.nav-tabs li');

            /** Set Wizard Step Information Placeholder */
            var numOfSteps  = wizardSteps.length-1;
            var currStepNum = 0;
            var prevStepNum = 0;
            var nextStepNum = 1;

            if(wizardSteps.length) {
                /** Find an set Current Step Number */
                wizardSteps.each( function(i,stepEle) {

                    var stepEleObj = $(stepEle);

                    if(stepEleObj.hasClass('active')) {
                        currStepNum = i;
                    }
                });

                /** Set Next Step Number */
                if((currStepNum+1) <= numOfSteps) {
                    nextStepNum = currStepNum+1;
                }

                /** Set Previous Step Number */
                if((currStepNum-1) >= 0) {
                    prevStepNum = currStepNum-1;
                }
            }

            /** Set Wizard Step Information to Settings */
            settings.numOfSteps  = numOfSteps;
            settings.currStepNum = currStepNum;
            settings.prevStepNum = prevStepNum;
            settings.nextStepNum = nextStepNum;
        };

        /** Internal Method: Set Initial Actions */
        var _setEventActions = function() {
            /** Set Initial Actions for jBsWizard Events */
            instance.on('jBsWizard.initializing', function () {
                /** Show jBsWizard Process Indicator Element */
                instance.showProcessing();
            });
            instance.on('jBsWizard.initialized' , function () {
                /** Hide jBsWizard Process Indicator Element */
                instance.hideProcessing();
            });
            instance.on('jBsWizard.prevStep'    , function () {
                instance.prevStep();
            });
            instance.on('jBsWizard.nextStep'    , function () {
                instance.nextStep();
            });
        };

        /** Internal Method: Set Initial Actions */
        var _updateProgressBar = function() {
            var steps = instance.steps();
            var numOfSteps  = steps.numOfSteps;
            var currStepNum = steps.currStepNum;
            var progressPercentage = (100 / numOfSteps) * currStepNum;

            var progressBar = thisWizardObj.find('.j-bs-wizard--progress .progress-bar');

            if(settings.useProgressBar) {
                progressBar.width(progressPercentage + '%').attr('aria-valuenow', progressPercentage);
            }
        };

        /** Public Method: Initialize jBsWizard */
        this.init = function() {
            /** If jBsWizard already initialized, destroy first */
            if(thisWizardObj.hasClass('js-bs-wizard--initialized')) {
                instance.destroy();
            }

            /** Set Initial Actions */
            _setEventActions();

            /** Add Main Class */
            thisWizardObj.addClass('j-bs-wizard');

            /** Trigger jBsWizard.initializing Event */
            instance.trigger('jBsWizard.initializing', [ instance ]);

            /** Wait a second */
            setTimeout(function () {
                /** Reset Step Position */
                var stepLinks = thisWizardObj.find('ul.nav-tabs li a');
                $(stepLinks[0]).click();

                /** Set Step Information */
                _setSteps();

                /** Add Classes */
                thisWizardObj.find('ul.nav-tabs').addClass('j-bs-wizard--steps');
                thisWizardObj.find('.j-bs-wizard--steps').find('li').addClass('j-bs-wizard--steps-item');
                thisWizardObj.find('.j-bs-wizard--steps-item').addClass('undone').find('a').attr('data-toggle','');
                thisWizardObj.find('.j-bs-wizard--steps-item.active').removeClass('undone').find('a').attr('data-toggle','tab');
                thisWizardObj.find('.tab-content').addClass('j-bs-wizard--steps-content');
                thisWizardObj.find('.j-bs-wizard--steps-content').find('.tab-pane').addClass('j-bs-wizard--steps-content-item');

                /** If use Progress Bar is enabled */
                if(settings.useProgressBar) {
                    /** Add Progress Bar Element */
                    var progressBarHtml = '<div class="progress j-bs-wizard--progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div></div>';
                    thisWizardObj.find('.j-bs-wizard--steps-content').prepend(progressBarHtml);
                    _updateProgressBar();
                }

                /** Add Prev / Next Buttons to Tab Content Items */
                var nextBtnHtml = '<span class="btn btn-primary j-bs-wizard--action-btn-next pull-right ' + settings.nextBtnAdditionalClass + '"><i class="j-bs-wizard--arrow j-bs-wizard--arrow-right" aria-hidden="true"></i> ' + settings.nextBtnStr + '</span>';
                var prevBtnHtml = '<span class="btn btn-default j-bs-wizard--action-btn-prev ' + settings.prevBtnAdditionalClass + '"><i class="j-bs-wizard--arrow j-bs-wizard--arrow-left" aria-hidden="true"></i> ' + settings.prevBtnStr + '</span>';
                thisWizardObj.find('.j-bs-wizard--steps-content-item').each( function(i,contentEle) {
                    var btnSetHtml = '<div class="j-bs-wizard--action-btn-set clearfix">';
                    if(i > 0) {
                        btnSetHtml += prevBtnHtml;
                    }
                    if(i < settings.numOfSteps) {
                        btnSetHtml += nextBtnHtml;
                    }
                    $(contentEle).append(btnSetHtml);
                });

                /** Add Events to Prev / Next Buttons */
                thisWizardObj.find('.j-bs-wizard--action-btn-next').click( function(e) {
                    e.preventDefault();
                    instance.trigger('jBsWizard.nextStep', [ instance ]);
                });
                thisWizardObj.find('.j-bs-wizard--action-btn-prev').click( function(e) {
                    e.preventDefault();
                    instance.trigger('jBsWizard.prevStep', [ instance ]);
                });

                /** Trigger Initialized and set Initialized Class */
                thisWizardObj.addClass('js-bs-wizard--initialized');
                instance.trigger('jBsWizard.initialized', [ instance ]);
            }, settings.waitASecondDelay);

            return instance;
        };

        /** Public Method: Destroy jBsWizard */
        this.destroy = function() {
            /** Trigger jBsWizard.destroy Event */
            instance.trigger('jBsWizard.destroy', [ instance ]);

            instance.showProcessing();

            /** unSet Steps */
            settings.numOfSteps  = 0;
            settings.currStepNum = 0;
            settings.prevStepNum = 0;
            settings.nextStepNum = 0;

            /** Remove Events from Prev / Next Buttons */
            thisWizardObj.find('.j-bs-wizard--action-btn-next').unbind();
            thisWizardObj.find('.j-bs-wizard--action-btn-prev').unbind();

            /** Remove Prev / Next Buttons from Tab Content Items */
            thisWizardObj.find('.j-bs-wizard--steps-content-item .j-bs-wizard--action-btn-set').remove();

            /** Remove Progress Bar Element */
            $('.j-bs-wizard--progress').remove();

            /** Remove Classes */
            thisWizardObj.find('.j-bs-wizard--steps-item').removeClass('undone').removeClass('done').find('a').attr('data-toggle','tab');
            thisWizardObj.find('.j-bs-wizard--steps li').removeClass('j-bs-wizard--steps-item');
            thisWizardObj.find('ul.nav-tabs').removeClass('j-bs-wizard--steps');
            thisWizardObj.find('.j-bs-wizard--steps-content .tab-pane').removeClass('j-bs-wizard--steps-content-item');
            thisWizardObj.find('.tab-content').removeClass('j-bs-wizard--steps-content');

            /** Remove Process Indicator Element */
            $('.j-bs-wizard--processing').remove();

            /** Remove Initialized CSS Class */
            thisWizardObj.removeClass('j-bs-wizard').removeClass('js-bs-wizard--initialized');

            /** Trigger jBsWizard.destroyed Event */
            instance.trigger('jBsWizard.destroyed', [ instance ]);

            /** Unbind Events for Initial Actions */
            instance.unbind();

            return this;
        };

        /** Public Method: Show jBsWizard Processing Indicator */
        this.showProcessing = function() {

            var wizardProcessingElement = $('.j-bs-wizard--processing');

            if(!wizardProcessingElement.length && thisWizardObj.hasClass('j-bs-wizard')) {
                /** Add Process Indicator Element */
                var processIndicatorHtml = '<div class="j-bs-wizard--processing panel panel-default"><div class="j-bs-wizard--spinner"><div class="j-bs-wizard--spinner1 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner2 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner3 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner4 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner5 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner6 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner7 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner8 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner9 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner10 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner11 j-bs-wizard--spinner-child"></div><div class="j-bs-wizard--spinner12 j-bs-wizard--spinner-child"></div></div><span class="sr-only">Loading...</span></div>';
                thisWizardObj.append(processIndicatorHtml);

                wizardProcessingElement = $('.j-bs-wizard--processing');
            }

            if(thisWizardObj.hasClass('j-bs-wizard') && wizardProcessingElement.length && !wizardProcessingElement.hasClass('show')) {

                instance.trigger('jBsWizard.showProcessing', [ instance ]);

                wizardProcessingElement.fadeTo( settings.processingFadeSpeed , 1, function() {
                    wizardProcessingElement.addClass('show');
                    wizardProcessingElement.removeAttr('style');
                });
            }
        };

        /** Public Method: Hide jBsWizard Processing Indicator */
        this.hideProcessing = function() {
            var wizardProcessingElement = $('.j-bs-wizard--processing');
            if(wizardProcessingElement.length && wizardProcessingElement.hasClass('show')) {

                instance.trigger('jBsWizard.hideProcessing', [ instance ]);

                wizardProcessingElement.fadeTo( settings.processingFadeSpeed , 0, function() {
                    wizardProcessingElement.removeClass('show');
                    wizardProcessingElement.removeAttr('style');
                });
            }
        };

        /** Public Method: Get jBsWizard Steps Information */
        this.steps = function() {
            var steps = {
                numOfSteps: settings.numOfSteps,
                currStepNum: settings.currStepNum,
                prevStepNum: settings.prevStepNum,
                nextStepNum: settings.nextStepNum
            };

            return steps;
        };

        /** Public Method: Go To Step by Step Number Number */
        this.goToStep = function(stepNum) {
            stepNum = typeof stepNum !== 'undefined' ? stepNum : 0;

            /** Set necessary Variables */
            var stepItems  = $('.j-bs-wizard--steps-item');


            var currentStepItem   = $(stepItems[settings.currStepNum]);
            var prevStepItem      = $(stepItems[settings.prevStepNum]);
            var goToStepItem      = null;
            var updateProgressBar = false;

            /** Is Step Num Valid */
            if((stepNum >= 0) && (stepNum <= settings.numOfSteps)) {
                /** Get Step Item */
                goToStepItem = $(stepItems[stepNum]);
            }

            /** If Step Item is found */
            if(goToStepItem !== null ) {
                if(stepNum !== settings.currStepNum) {
                    /** Update Step Numbers */
                    settings.currStepNum = stepNum;
                    settings.prevStepNum = ((stepNum-1) >= 0) ? stepNum-1 : 0;
                    settings.nextStepNum = ((stepNum+1) <= settings.numOfSteps) ? stepNum+1 : settings.numOfSteps;

                    if(!goToStepItem.hasClass('done')) {
                        updateProgressBar = true;
                    }
                    /** Activate Tab */
                    goToStepItem.removeClass('undone').find('a').attr('data-toggle', 'tab');

                    /** Go to Tab */
                    var goToStepItemHref = goToStepItem.find('a').attr('href');
                    $('[href="' + goToStepItemHref + '"]').tab('show');

                    /** Add Done Class to current (now previous) Item */
                    currentStepItem.removeClass('undone').addClass('done');

                    if(updateProgressBar) {
                        _updateProgressBar();
                    }

                    instance.trigger('jBsWizard.goToStep', [instance, instance.steps()]);
                }
            }
        };

        /** Public Method: Go To Next Step */
        this.nextStep = function() {
            /** Show jBsWizard Process Indicator Element */
            instance.showProcessing();

            /** Wait a second */
            setTimeout(function () {
                /** Go To Step */
                instance.goToStep(settings.nextStepNum);

                /** Hide jBsWizard Process Indicator Element */
                instance.hideProcessing();
            }, settings.waitASecondDelay);
        };

        /** Public Method: Go To Previous Step */
        this.prevStep = function() {
            instance.goToStep(settings.prevStepNum);
        };

        /** Get Tabs Show Event to Update Steps */
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if($(this).parent().hasClass('undone')) {
                // do nothing...
            } else {
                _setSteps();
            }
        });

        /** Check if there are Steps */
        if($('ul.nav-tabs li').length) {
            /** Initialize jBsWizard */
            return instance.init();
        } else {
            /** Return jBsWizard Warning */
            console.warn('jBsWizard Error:','Cannot find Steps for init Wizard');
        }
    }
} (jQuery));